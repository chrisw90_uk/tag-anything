const express = require("express");
const router = express.Router();
const Thing = require('../models/Thing');

router.get('/', (req, res) => {
    const { name, tags } = req.query;
    let filter = {
        name: { $regex: name, $options: 'i' },
    };
    if (tags) {
        filter = {
            ...filter,
            tags: { $all: tags.split(',') }
        };
    }
    Thing
        .find(filter)
        .populate('tags')
        .sort('-created_at')
        .exec((error, data) => {
            if (error) return res.json(error);
            res.json(data)
        });
});

router.post('/', (req, res) => {
    const { name, tags } = req.body;
    const thing = new Thing({ name, tags: tags.map(t => t._id) });
    thing.save((error, data) => {
        if (error) return res.json(error);
        return res.json({
            ...data._doc,
            tags,
        });
    });
});

module.exports = router;