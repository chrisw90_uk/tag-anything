const express = require("express");
const router = express.Router();
const Tag = require('../models/Tag');

router.get('/', (req, res) => {
    const { searchTerm } = req.query;
    Tag.find(
        { name: { $regex: searchTerm, $options: 'i' }},
        null,
        { limit: 8 },
     (err, data) => res.json(data)
    );
});

router.post('/', (req, res) => {
    const { name } = req.body;
    const tag = new Tag({ name });
    tag.save((error, data) => {
        if (error) return res.json(error);
        return res.json(data);
    });
});

router.delete('/', (req, res) => {
    const { id } = req.query;
    Tag.findByIdAndDelete(id,(err, data) => res.json({ message: 'Tag deleted successfully' }));
});

module.exports = router;