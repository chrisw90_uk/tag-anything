const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ThingSchema = Schema({
    name: {
        type: String,
        required: true
    },
    tags: [{
        type: Schema.Types.ObjectId,
        ref: "Tags"
    }],
    created_at: {
        type: Date,
        default: Date.now
    },
    deleted_at: {
        type: Date,
        default: null
    }
});

module.exports = mongoose.model('Things', ThingSchema);