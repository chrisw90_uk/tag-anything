const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TagSchema = Schema({
    name: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    deleted_at: {
        type: Date,
        default: null
    }
});

module.exports = mongoose.model('Tags', TagSchema);