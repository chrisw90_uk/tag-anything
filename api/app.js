require("dotenv/config");
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use(cors());

const tagsRouter = require('./routes/tags');
const thingsRouter = require('./routes/things');

app.use('/api/tags', tagsRouter);
app.use('/api/things', thingsRouter);

mongoose.connect(
    process.env.DB_CONNECTION,
    { useNewUrlParser: true, useUnifiedTopology: true },
).catch(error => console.log(error));

app.listen(9000);