import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import configureMockStore from 'redux-mock-store'
import { Provider } from "react-redux";
import { initialState } from "../../reducers";

import TagFilterInput, { mapDispatchToProps } from "../../containers/TagFilterInput";
import thunk from "redux-thunk";
import * as actions from "../../actions";

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const props = {
    id: 'tags',
    label: 'Tags',
    tagResults: [],
    tags: [{ _id: 1, name: 'test tag 1', isNew: true }],
    fetchTags: () => null,
    setTags: () => {},
    addTag: () => null,
};

describe('TagFilterInput', () => {

    it('should dispatch setTags() when called', () => {
        const dispatch = jest.fn();
        mapDispatchToProps(dispatch).setTags([]);
        expect(dispatch.mock.calls[0][0]).toEqual({
            type: 'SET_TABLE_ARRAY_FILTER',
            payload: { field: 'tags', value: [] }}
        );
    });

    it('should dispatch fetchTags() when called', () => {
        const store = mockStore({
            results: initialState
        });
        render(
            <Provider store={store}>
                <TagFilterInput {...props}/>
            </Provider>
        );
        const input = screen.getByLabelText('Tags');
        fireEvent.change(input, { target: { value: 'tag' } });
        const expectedActions = [
            { type: 'FETCH_TAGS_FOR_FILTER_START' },
        ];
        expect(store.getActions()).toEqual(expectedActions)
    });
});
