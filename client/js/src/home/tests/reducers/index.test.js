import reducer, { initialState } from '../../reducers';

describe('Results reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });
    it('should handle fetching results', () => {
        const data = ['result 1', 'result 2'];
        let result = reducer(undefined, { type: 'FETCH_THINGS_START' });
        expect(result.isFetchingResults).toBe(true);
        result = reducer(undefined, { type: 'FETCH_THINGS_SUCCESS', payload: { data } });
        expect(result.isFetchingResults).toBe(false);
        expect(result.results).toBe(data);
    });
    it('should handle failing to fetch results', () => {
        let result = reducer(undefined, { type: 'FETCH_THINGS_START' });
        expect(result.isFetchingResults).toBe(true);
        result = reducer(undefined, { type: 'FETCH_THINGS_FAIL' });
        expect(result.isFetchingResults).toBe(false);
    });
    it('should handle new thing being added', () => {
        const data = 'result 3';
        const result = reducer({
            ...initialState,
            results: ['result 1', 'result 2'],
        }, { type: 'ADD_THING_SUCCESS', payload: { data } });
        expect(result.results).toHaveLength(3);
    });
    it('should handle result filters', () => {
        let result = reducer(undefined, { type: 'UPDATE_TABLE_TEXT_FILTER', payload: { field: 'name', value: 'test' } });
        expect(result.filters.name).toBe('test');
        result = reducer(undefined, { type: 'SET_TABLE_ARRAY_FILTER', payload: { field: 'tags', value: ['tag 1', 'tag 2'] } });
        expect(result.filters.tags).toHaveLength(2);
    });
    it('should handle fetching tags', () => {
        const data = ['tag 1', 'tag 2'];
        let result = reducer(undefined, { type: 'FETCH_TAGS_FOR_FILTER_START' });
        expect(result.filters.isFetchingTags).toBe(true);
        result = reducer(undefined, { type: 'FETCH_TAGS_FOR_FILTER_SUCCESS', payload: { data } });
        expect(result.filters.isFetchingTags).toBe(false);
        expect(result.filters.tagResults).toBe(data);
    });
    it('should handle failing to fetch tags', () => {
        let result = reducer(undefined, { type: 'FETCH_TAGS_FOR_FILTER_START' });
        expect(result.filters.isFetchingTags).toBe(true);
        result = reducer(undefined, { type: 'FETCH_TAGS_FOR_FILTER_FAIL' });
        expect(result.filters.isFetchingTags).toBe(false);
    });
});