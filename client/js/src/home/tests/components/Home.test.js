import React from 'react';
import { render, screen } from '../../../test-utils'
import '@testing-library/jest-dom/extend-expect'

import Home from '../../components/Home';

describe('Home', () => {
    it('renders', () => {
        render(<Home />);
        expect(screen.getByText(/things/i)).toBeDefined();
    });
});