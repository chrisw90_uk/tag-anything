import React from 'react';
import { render, fireEvent, screen, } from '../../../test-utils'
import '@testing-library/jest-dom/extend-expect'

import ThingsTable from "../../components/ThingsTable";

const props = {
    isFetchingResults: false,
    fetchThings: () => null,
    results: [{
        _id: 1,
        name: 'thing 1',
        tags: [{
            _id: 1,
            name: 'test tag',
        }]
    }]
};

describe('ThingsTable', () => {
    it('renders loading state', () => {
        render(<ThingsTable {...props} isFetchingResults />);
        expect(screen.getByAltText('Loading')).toBeInTheDocument();
    });
    it('renders things', () => {
        render(<ThingsTable {...props} />);
        expect(screen.getByText('thing 1')).toBeDefined();
        expect(screen.getByText('test tag')).toBeDefined();
    });
    it('renders dash when thing has no tags', () => {
        render(<ThingsTable {...props} results={[{ _id: 1, name: 'thing 1', tags: [] }]} />);
        const resultTableCells = screen.getAllByRole('cell');
        expect(resultTableCells[resultTableCells.length - 1]).toContainHTML('&mdash;');
    });
});