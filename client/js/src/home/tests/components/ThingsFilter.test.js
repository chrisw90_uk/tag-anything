import React from 'react';
import { render, fireEvent, screen, } from '../../../test-utils'
import '@testing-library/jest-dom/extend-expect'

import ThingsFilter from "../../containers/ThingsFilter";

describe('ThingsFilter', () => {
    it('allows user to name filter', () => {
        render(<ThingsFilter />);
        const input = screen.getByLabelText('Name');
        fireEvent.change(input, { target: { value: 'test name' } });
        expect(input.value).toBe('test name');
    });
});
