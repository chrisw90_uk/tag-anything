import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../../actions'
import { initialState } from '../../reducers';
import axios from "axios";

jest.mock('axios');

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const thingData = [
    { _id: 1, name: 'thing 1', tags: [] },
    { _id: 1, name: 'thing 2', tags: [] },
];

const tagData = [
    { _id: 1, name: 'tag 1' },
    { _id: 1, name: 'tag 2' },
];

describe('Home actions', () => {

    it('fetches things successfully', async() => {
        axios.get.mockResolvedValue({ data: thingData });
        const expectedActions = [
            { type: 'FETCH_THINGS_START' },
            { type: 'FETCH_THINGS_SUCCESS', payload: { data: thingData } }
        ];
        const store = mockStore({
            results: initialState
        });
        await store.dispatch(actions.fetchThings());
        expect(store.getActions()).toEqual(expectedActions)
    });

    it('fails to fetch things', async() => {
        axios.get.mockRejectedValueOnce('error');
        const expectedActions = [
            { type: 'FETCH_THINGS_START' },
            { type: 'FETCH_THINGS_FAIL', payload: { err: 'error' } }
        ];
        const store = mockStore({
            results: initialState
        });
        await store.dispatch(actions.fetchThings());
        expect(store.getActions()).toEqual(expectedActions)
    });

    it('fetches tags for filter successfully', async() => {
        axios.get.mockResolvedValue({ data: tagData });
        const expectedActions = [
            { type: 'FETCH_TAGS_FOR_FILTER_START' },
            { type: 'FETCH_TAGS_FOR_FILTER_SUCCESS', payload: { data: tagData } }
        ];
        const store = mockStore({
            results: initialState
        });
        await store.dispatch(actions.fetchTagsForFilter());
        expect(store.getActions()).toEqual(expectedActions)
    });

    it('fails fetches tags for filter', async() => {
        axios.get.mockRejectedValueOnce('error');
        const expectedActions = [
            { type: 'FETCH_TAGS_FOR_FILTER_START' },
            { type: 'FETCH_TAGS_FOR_FILTER_FAIL', payload: { err: 'error' } }
        ];
        const store = mockStore({
            results: initialState
        });
        await store.dispatch(actions.fetchTagsForFilter());
        expect(store.getActions()).toEqual(expectedActions)
    });

    it('updates name filter', async() => {
        const expectedActions = [
            { type: 'UPDATE_TABLE_TEXT_FILTER', payload: { field: 'name', value: 'test value'} },
        ];
        const store = mockStore({
            results: initialState
        });
        await store.dispatch(actions.updateTextFilter('name', 'test value'));
        expect(store.getActions()).toEqual(expectedActions);
    });
});