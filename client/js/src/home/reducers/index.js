export const initialState = {
    isFetchingResults: true,
    filters: {
        isFetchingTags: false,
        name: '',
        tags: [],
        tagResults: [],
    },
    results: [],
};

const ResultsReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_THINGS_START':
            return {
                ...state,
                isFetchingResults: true,
            };
        case 'FETCH_THINGS_SUCCESS':
            return {
                ...state,
                isFetchingResults: false,
                results: action.payload.data,
            };
        case 'FETCH_THINGS_FAIL':
            return {
                ...state,
                isFetchingResults: false,
            };
        case 'ADD_THING_SUCCESS':
            return {
                ...state,
                results: [action.payload.data, ...state.results],
            };
        case 'UPDATE_TABLE_TEXT_FILTER':
            return {
                ...state,
                filters: {
                    ...state.filters,
                    [action.payload.field]: action.payload.value
                }
            };
        case 'SET_TABLE_ARRAY_FILTER':
            return {
                ...state,
                filters: {
                    ...state.filters,
                    [action.payload.field]: action.payload.value,
                }
            };
        case 'FETCH_TAGS_FOR_FILTER_START':
            return {
                ...state,
                filters: {
                    ...state.filters,
                    isFetchingTags: true,
                }
            };
        case 'FETCH_TAGS_FOR_FILTER_SUCCESS':
            return {
                ...state,
                filters: {
                    ...state.filters,
                    isFetchingTags: false,
                    tagResults: action.payload.data,
                }
            };
        case 'FETCH_TAGS_FOR_FILTER_FAIL':
            return {
                ...state,
                filters: {
                    ...state.filters,
                    isFetchingTags: false,
                }
            };
        default:
            return state;
    }
};

export default ResultsReducer;