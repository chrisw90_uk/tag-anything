import React from 'react';
import loading from '../../../../images/spinner-orange.gif';

const ThingsLoadingTable = () => (
    <div className="position--relative">
        <img
            className="table__spinner"
            src={loading}
            alt="Loading"
        />
        <table className="table table--loading">
            <thead>
            <tr>
                <th>Name</th>
                <th>Category</th>
                <th>Subcategory</th>
                <th>Tags</th>
            </tr>
            </thead>
            <tbody>
            {[1, 2, 3, 4, 5].map(el => (
                <tr key={el}>
                    <td>Lorem ipsum dolor sit amet</td>
                    <td>&mdash;</td>
                    <td>&mdash;</td>
                    <td>
                        <ul className="list--reset tags__list">
                            <li className="tags__list-item">lorem</li>
                            <li className="tags__list-item">ipsum</li>
                            <li className="tags__list-item">dolor</li>
                        </ul>
                    </td>
                </tr>
            ))}
            </tbody>
        </table>
    </div>
);

export default ThingsLoadingTable;