import React, { useEffect } from 'react';
import InputField from "../../components/InputField";
import TagFilterInput from "../containers/TagFilterInput";

const ThingsFilter = ({
    fetchThings,
    updateTextFilter,
    name,
    tags,
}) => {
    useEffect(() => {
        fetchThings();
    }, [name, tags]);
    return (
        <div className="filter">
            <h3>Filters</h3>
            <InputField
                id="nameFilter"
                label="Name"
                value={name}
                onChange={e => updateTextFilter('name', e.target.value)}
            />
            <TagFilterInput
                id="tagsFilter"
                label="Tags"
                tags={tags}
                disableAddNewTag
            />
        </div>
    )
};

export default ThingsFilter;
