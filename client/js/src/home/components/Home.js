import React, { useEffect } from 'react';
import Button from '../../components/Button';
import AddThing from "../../thing/containers/AddThing";
import ThingsFilter from "../containers/ThingsFilter";
import ThingsTable from "../containers/ThingsTable";

const Home = () => (
    <div className="layout flex">
        <aside>
            <div className="card">
                <AddThing />
            </div>
        </aside>
        <main>
            <div className="card">
                <h2>Things</h2>
                <ThingsFilter />
                <ThingsTable />
            </div>
        </main>
    </div>
);

export default Home;