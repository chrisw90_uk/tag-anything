import React, { useEffect } from 'react';
import ThingsLoadingTable from "./ThingsLoadingTable";
import loading from "../../../../images/spinner-orange.gif";

const ThingsTable = ({
    isFetchingResults,
    fetchThings,
    results,
}) => {
    useEffect(() => {
        fetchThings();
    }, []);
    return (
        isFetchingResults && results.length === 0 ? (
            <ThingsLoadingTable />
        ) : (
            <div className="position--relative">
                {isFetchingResults && (
                    <img
                        className="table__spinner"
                        src={loading}
                        alt="Loading"
                    />
                )}
                <table className={`table${isFetchingResults ? ' table--loading' : ''}`}>
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Subcategory</th>
                        <th>Tags</th>
                    </tr>
                    </thead>
                    <tbody>
                    {results.map(thing => (
                        <tr key={thing._id}>
                            <td>{thing.name}</td>
                            <td>&mdash;</td>
                            <td>&mdash;</td>
                            <td>
                                {thing.tags.length === 0 ? (
                                    <React.Fragment>&mdash;</React.Fragment>
                                ) : (
                                    <ul className="list--reset tags__list">
                                        {thing.tags.map(tag => <li key={tag._id} className="tags__list-item">{tag.name}</li>)}
                                    </ul>
                                )}
                            </td>
                        </tr>
                    ))}
                    </tbody>
                </table>
            </div>
        )
    )
};

export default ThingsTable;