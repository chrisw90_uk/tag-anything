import axios from 'axios';
const api = 'http://localhost:9000/api';

const fetchThingsStart = () => ({
    type: 'FETCH_THINGS_START',
});
const fetchThingsSuccess = data => ({
    type: 'FETCH_THINGS_SUCCESS',
    payload: {
        data,
    }
});
const fetchThingsFail = err => ({
    type: 'FETCH_THINGS_FAIL',
    payload: { err },
});

export const fetchThings = () => async (dispatch, getState) => {
    dispatch(fetchThingsStart());
    const { name, tags } = getState().results.filters;
    try {
        const { data } = await axios.get(`${api}/things?name=${name}&tags=${tags.map(t => t._id)}`);
        dispatch(fetchThingsSuccess(data));
    } catch (e) {
        dispatch(fetchThingsFail(e));
    }
};

const fetchTagsStart = () => ({
    type: 'FETCH_TAGS_FOR_FILTER_START',
});
const fetchTagsSuccess = data => ({
    type: 'FETCH_TAGS_FOR_FILTER_SUCCESS',
    payload: {
        data,
    }
});
const fetchTagsFail = err => ({
    type: 'FETCH_TAGS_FOR_FILTER_FAIL',
    payload: { err },
});

export const fetchTagsForFilter = value => async (dispatch) => {
    dispatch(fetchTagsStart());
    try {
        const { data } = await axios.get(`${api}/tags?searchTerm=${value}`);
        return dispatch(fetchTagsSuccess(data));
    } catch (e) {
        return dispatch(fetchTagsFail(e));
    }
};

export const updateTextFilter = (field, value) => ({
    type: 'UPDATE_TABLE_TEXT_FILTER',
    payload: { field, value }
});

export const setTableArrayFilter = (field, value) => ({
    type: 'SET_TABLE_ARRAY_FILTER',
    payload: { field, value }
});
