import React from 'react';
import { connect } from 'react-redux'
import {
    fetchThings
} from "../actions";
import ThingsTable from '../components/ThingsTable';

const mapStateToProps = state => ({
    isFetchingResults: state.results.isFetchingResults,
    results: state.results.results,
});

const mapDispatchToProps = {
    fetchThings
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ThingsTable);