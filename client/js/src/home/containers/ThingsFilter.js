import React from 'react';
import { connect } from 'react-redux'
import {
    fetchThings,
    updateTextFilter,
} from "../actions";
import ThingsFilter from '../components/ThingsFilter';

const mapStateToProps = state => ({
    name: state.results.filters.name,
    tags: state.results.filters.tags,
});

const mapDispatchToProps = {
    fetchThings,
    updateTextFilter,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ThingsFilter);