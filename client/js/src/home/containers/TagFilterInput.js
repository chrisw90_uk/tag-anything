import { connect } from 'react-redux'
import {
    fetchTagsForFilter,
    setTableArrayFilter
} from "../actions";
import TagField from '../../tags/components/TagField';

const mapStateToProps = state => ({
    isFetchingTags: state.results.filters.isFetchingTags,
    tagResults: state.results.filters.tagResults,
});

export const mapDispatchToProps = dispatch => ({
    fetchTags: value => dispatch(fetchTagsForFilter(value)),
    setTags: tags => dispatch(setTableArrayFilter('tags', tags)),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TagField);
