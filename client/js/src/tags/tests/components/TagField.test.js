import React from 'react';
import { render, fireEvent, screen, waitFor } from '../../../test-utils'
import '@testing-library/jest-dom/extend-expect'

import TagField from "../../components/TagField";

const props = {
    tagResults: [],
    tags: [{ _id: 1, name: 'test tag 1', isNew: true }, { _id: 2, name: 'test tag 2' }],
    fetchTags: () => null,
    setTags: () => {},
    addTag: () => null,
};

describe('TagField', () => {
    it('allows user to search/enter a tag name', () => {
        render(<TagField {...props} />);
        const input = screen.getByRole('textbox');
        fireEvent.change(input, { target: { value: 'tag name' } });
        expect(input.value).toBe('tag name');
    });

    it('maps over current tags', () => {
        render(<TagField {...props} />);
        expect(screen.getByText('test tag 1')).toBeDefined();
    });

    it('removes existing tag', async () => {
        const handleClick = jest.fn();
        const handleDelete = jest.fn();
        render(<TagField {...props} setTags={handleClick} deleteTag={handleDelete} />);
        const firstTag = screen.getByText('test tag 1');
        expect(firstTag).toBeDefined();
        const firstTagDelBtn = screen.getAllByText('cancel')[0].closest('button');
        fireEvent.click(firstTagDelBtn);
        expect(handleClick).toBeCalledTimes(1);
        expect(handleDelete).toBeCalledTimes(1);
    });

    it('does not remove existing tag if tag is not new', async () => {
        const handleClick = jest.fn();
        const handleDelete = jest.fn();
        render(<TagField {...props} setTags={handleClick} deleteTag={handleDelete} />);
        const secondTag = screen.getByText('test tag 2');
        expect(secondTag).toBeDefined();
        const secondTageDelBtn = screen.getAllByText('cancel')[1].closest('button');
        fireEvent.click(secondTageDelBtn);
        expect(handleClick).toBeCalledTimes(1);
        expect(handleDelete).toBeCalledTimes(0);
    });

    it('clears input when new tag is added', async () => {
        render(<TagField {...props} />);
        const input = screen.getByRole('textbox');
        fireEvent.change(input, { target: { value: 'tag name' } });
        expect(input.value).toBe('tag name');
        const newTagBtn = screen.getByText('Add "tag name"');
        fireEvent.click(newTagBtn);
        await waitFor(() => expect(input.value).toBe(''));
    });

    it('add new tag from tag pool and clear input', async () => {
        render(<TagField {...props} tagResults={[{ _id: 2, name: 'test tag 2' }, { _id: 3, name: 'test tag 3' }]} />);
        const input = screen.getByRole('textbox');
        fireEvent.change(input, { target: { value: 'tag name' } });
        const tagPoolBtn = screen.getByText('test tag 3');
        fireEvent.click(tagPoolBtn);
        await waitFor(() => expect(input.value).toBe(''));
    });
});
