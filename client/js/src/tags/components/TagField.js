import React, { useState, useRef } from 'react';
import debounce from 'lodash.debounce';

const TagInput = ({
    id,
    label,
    isFetchingTags,
    disabled,
    tags,
    setTags,
    tagResults,
    fetchTags,
    addTag,
    deleteTag,
    disableAddNewTag,
}) => {
    const inputRef = useRef(null);
    const [newTag, setNewTag] = useState('');
    const uniqueTagResults = tagResults.filter(t => !tags.some(tag => tag._id === t._id));
    const canAddNewTag = !tags.some(t => t.name.toLowerCase() === newTag.toLowerCase()) && !tagResults.some(t => t.name.toLowerCase() === newTag.toLowerCase());
    return (
        <div className="form__field">
            <label htmlFor={id}>
                {label}
            </label>
            <div className="tags">
                <ul className="list--reset tags__list">
                    {tags.map(t => (
                        <li
                            key={t._id}
                            className="tags__list-item"
                        >
                            {t.name}
                            <button
                                onClick={() => {
                                    setTags(tags.filter(tag => tag._id !== t._id));
                                    if (deleteTag && t.isNew) deleteTag(t._id);
                                }}
                                className="btn--reset btn__icon"
                            >
                                <i className="material-icons">cancel</i>
                            </button>
                        </li>
                    ))}
                    <li
                        className="tags__list-input"
                    >
                        <input
                            id={id}
                            ref={inputRef}
                            className="input--reset"
                            value={newTag}
                            disabled={disabled}
                            onChange={e => {
                                const val = e.target.value;
                                setNewTag(val);
                                val.length >= 2 && fetchTags(e.target.value);
                            }}
                        />
                    </li>
                </ul>
                {newTag.length >= 2 && (
                    <ul className="list--reset tags__dropdown">
                        {(!disableAddNewTag && canAddNewTag) && (
                            <li className="tags__dropdown-item">
                                <button
                                    onClick={async () => {
                                        const savedTag = await addTag(newTag);
                                        setTags([...tags, savedTag]);
                                        setNewTag('');
                                        inputRef.current.focus();
                                    }}
                                    className="btn--reset">
                                    {`Add "${newTag}"`}
                                </button>
                            </li>
                        )}
                        {uniqueTagResults.map(t => (
                            <li className="tags__dropdown-item" key={t._id}>
                                <button
                                    onClick={() => {
                                        setTags([...tags, t]);
                                        setNewTag('');
                                        inputRef.current.focus();
                                    }}
                                    className="btn--reset"
                                >
                                    {t.name}
                                </button>
                            </li>
                        ))}
                    </ul>
                )}
            </div>
        </div>
    )
};

export default TagInput;