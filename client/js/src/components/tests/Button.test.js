import React from 'react';
import { render, screen } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

import Button from '../Button';

const props = {
    onClick: () => {},
};

describe('Button', () => {
    it('renders', () => {
        render(<Button {...props}>Click Me!</Button>);
        expect(screen.getByText(/click me!/i)).toBeDefined();
    });
    it('renders with loading state', () => {
        render(<Button {...props} showLoadingState>Click Me!</Button>);
        expect(screen.getByAltText(/loading/i)).toBeDefined();
    })
});