import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

import InputField from "../InputField";

const props = {
    label: 'Name',
    value: 'hello',
    onChange: () => null,
};

describe('InputField', () => {
    it('renders with label', () => {
        render(<InputField {...props} />);
        expect(screen.getByText('Name')).toBeDefined();
    });
    it('displays value', () => {
        render(<InputField {...props} />);
        expect(screen.getByRole('textbox').value).toBe('hello');
    });
});