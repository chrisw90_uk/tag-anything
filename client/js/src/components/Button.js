import React from 'react';
import loading from '../../../images/spinner.gif';

const Button = ({
    showLoadingState,
    onClick,
    disabled,
    children
}) => (
    <button
        className={`btn${showLoadingState ? ' btn--loading' : ''}`}
        onClick={onClick}
        disabled={disabled}
    >
        {showLoadingState && (
            <img
                className="btn__spinner"
                src={loading}
                alt="Loading"
            />
        )}
        <span className="btn__content">{children}</span>
    </button>
);

export default Button;