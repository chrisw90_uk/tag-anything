import React from 'react';

const InputField = ({
    id,
    type = 'text',
    label,
    value,
    onChange,
    disabled,
}) => (
    <div className="form__field">
        <label
            htmlFor={id}
        >
            {label}
        </label>
        <input
            id={id}
            type={type}
            value={value}
            onChange={onChange}
            disabled={disabled}
        />
    </div>
);

export default InputField;