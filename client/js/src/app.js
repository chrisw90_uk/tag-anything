import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from './store';
import Home from './home/components/Home';

import style from '../../scss/style.scss';

ReactDOM.render(
    <Provider store={ store }>
        <Home />
    </Provider>,
    document.getElementById('app')
);
