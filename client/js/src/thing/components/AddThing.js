import React, { useState } from 'react';
import InputField from '../../components/InputField';
import Button from "../../components/Button";
import TagInput from "../containers/TagField";

const AddThing = ({ isSavingNewThing, addThing }) => {
    const [name, setName] = useState('');
    const [tags, setTags] = useState([]);
    return (
        <React.Fragment>
            <h2>Add Thing</h2>
            <div className="form">
                <InputField
                    id="thingName"
                    label="Name"
                    type="text"
                    value={name}
                    onChange={e => setName(e.target.value)}
                    disabled={isSavingNewThing}
                />
                <TagInput
                    id="thingTags"
                    label="Tags"
                    tags={tags}
                    setTags={setTags}
                    disabled={isSavingNewThing}
                />
                <div className="text--right">
                    <Button
                        disabled={!name || isSavingNewThing}
                        type="button"
                        onClick={async () => {
                            const success = await addThing({ name, tags });
                            if (success) {
                                setName('');
                                setTags([]);
                            }
                        }}
                        showLoadingState={isSavingNewThing}
                    >
                        Add
                    </Button>
                </div>
            </div>
        </React.Fragment>
    )
};

export default AddThing;