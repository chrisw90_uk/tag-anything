import React from 'react';
import { connect } from 'react-redux'
import {
    addThing,
} from "../../thing/actions";
import AddThing from '../components/AddThing';

const mapStateToProps = state => ({
    isSavingNewThing: state.thing.isSavingNewThing,
});

const mapDispatchToProps = { addThing };

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AddThing);