import React from 'react';
import { connect } from 'react-redux'
import {
    fetchTags,
    addTag,
    deleteNewTag,
} from "../../thing/actions";
import TagField from '../../tags/components/TagField';

const mapStateToProps = state => ({
    isFetchingTags: state.thing.isFetchingTags,
    tagResults: state.thing.tags,
});

const mapDispatchToProps = {
    addTag,
    fetchTags,
    deleteTag: deleteNewTag,
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(TagField);