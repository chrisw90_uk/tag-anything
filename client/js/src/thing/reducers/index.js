export const initialState = {
    isCreatingNewThing: true,
    isSavingNewThing: false,
    isFetchingTags: false,
    isSavingNewTag: false,
    name: '',
    tags: [],
};

const ThingReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_TAGS_START':
            return {
                ...state,
                isFetchingTags: true,
            };
        case 'FETCH_TAGS_SUCCESS':
            return {
                ...state,
                isFetchingTags: false,
                tags: action.payload.tags,
            };
        case 'FETCH_TAGS_FAIL':
            return {
                ...state,
                isFetchingTags: false,
            };
        case 'ADD_TAG_START':
            return {
                ...state,
                isSavingNewTag: true,
            };
        case 'ADD_TAG_SUCCESS':
        case 'ADD_TAG_FAIL':
            return {
                ...state,
                isSavingNewTag: false,
            };
        case 'ADD_THING_START':
            return {
                ...state,
                isSavingNewThing: true,
            };
        case 'ADD_THING_SUCCESS':
            return {
                ...state,
                isSavingNewThing: false,
                name: '',
                tags: [],
            };
        case 'ADD_THING_FAIL':
            return {
                ...state,
                isSavingNewThing: false,
            };
        default:
            return state;
    }
};

export default ThingReducer;