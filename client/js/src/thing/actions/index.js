import axios from 'axios';
const api = 'http://localhost:9000/api';

const fetchTagsStart = () => ({
    type: 'FETCH_TAGS_START',
});
const fetchTagsSuccess = tags => ({
    type: 'FETCH_TAGS_SUCCESS',
    payload: { tags },
});
const fetchTagsFail = err => ({
    type: 'FETCH_TAGS_FAIL',
    payload: { err },
});

export const fetchTags = value => async (dispatch) => {
    dispatch(fetchTagsStart());
    try {
        const { data } = await axios.get(`${api}/tags?searchTerm=${value}`);
        dispatch(fetchTagsSuccess(data));
    } catch (e) {
        dispatch(fetchTagsFail(e));
    }
};

const addTagStart = () => ({
    type: 'ADD_TAG_START',
});
const addTagSuccess = () => ({
    type: 'ADD_TAG_SUCCESS',
});
const addTagFail = err => ({
    type: 'ADD_TAG_FAIL',
    payload: { err },
});

export const addTag = name => async (dispatch) => {
    dispatch(addTagStart());
    try {
        const { data } = await axios.post(`${api}/tags`, { name });
        dispatch(addTagSuccess());
        return { ...data, isNew: true };
    } catch (e) {
        dispatch(addTagFail(e));
    }
};

const deleteNewTagStart = () => ({
    type: 'DELETE_NEW_TAG_START',
});
const deleteNewTagSuccess = () => ({
    type: 'DELETE_NEW_TAG_SUCCESS',
});
const deleteNewTagFail = err => ({
    type: 'DELETE_NEW_TAG_FAIL',
    payload: { err },
});

export const deleteNewTag = id => async (dispatch) => {
    dispatch(deleteNewTagStart());
    try {
        await axios.delete(`${api}/tags?id=${id}`);
        dispatch(deleteNewTagSuccess());
    } catch (e) {
        dispatch(deleteNewTagFail(e));
    }
};

const addThingStart = () => ({
    type: 'ADD_THING_START',
});
const addThingSuccess = data => ({
    type: 'ADD_THING_SUCCESS',
    payload: {
        data,
    }
});
const addThingFail = err => ({
    type: 'ADD_THING_FAIL',
    payload: { err },
});

export const addThing = ({ name, tags }) => async (dispatch) => {
    dispatch(addThingStart());
    try {
        const { data } = await axios.post(`${api}/things`, { name, tags });
        dispatch(addThingSuccess(data));
        return true;
    } catch (e) {
        dispatch(addThingFail(e));
        return false;
    }
};