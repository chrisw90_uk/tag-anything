import configureMockStore from 'redux-mock-store'
import thunk from 'redux-thunk'
import * as actions from '../../actions'
import { initialState } from '../../reducers';
import axios from "axios";

jest.mock('axios');

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const tagData = [
    { _id: 1, name: 'tag 1' },
    { _id: 2, name: 'tag 2' },
];

const thingData = {
    name: 'new thing',
    tags: tagData.map(t => t._id),
};

describe('Thing actions', () => {

    it('fetches tags successfully', async () => {
        axios.get.mockResolvedValue({ data: tagData });
        const expectedActions = [
            { type: 'FETCH_TAGS_START' },
            { type: 'FETCH_TAGS_SUCCESS', payload: { tags: tagData } }
        ];
        const store = mockStore({
            results: initialState
        });
        await store.dispatch(actions.fetchTags(''));
        expect(store.getActions()).toEqual(expectedActions)
    });

    it('fails to fetch tags', async () => {
        axios.get.mockRejectedValueOnce('error');
        const expectedActions = [
            { type: 'FETCH_TAGS_START' },
            { type: 'FETCH_TAGS_FAIL', payload: { err: 'error' } }
        ];
        const store = mockStore({
            results: initialState
        });
        await store.dispatch(actions.fetchTags(''));
        expect(store.getActions()).toEqual(expectedActions)
    });

    it('adds new tag successfully', async () => {
        axios.post.mockResolvedValue({ data: tagData });
        const expectedActions = [
            { type: 'ADD_TAG_START' },
            { type: 'ADD_TAG_SUCCESS' },
        ];
        const store = mockStore({
            results: initialState
        });
        await store.dispatch(actions.addTag(''));
        expect(store.getActions()).toEqual(expectedActions)
    });

    it('fails to add new tag', async () => {
        axios.post.mockRejectedValueOnce('error');
        const expectedActions = [
            { type: 'ADD_TAG_START' },
            { type: 'ADD_TAG_FAIL', payload: { err: 'error' } }
        ];
        const store = mockStore({
            results: initialState
        });
        await store.dispatch(actions.addTag(''));
        expect(store.getActions()).toEqual(expectedActions)
    });

    it('delete new tag successfully', async () => {
        axios.delete.mockResolvedValue({ message: 'Tag deleted successfully' });
        const expectedActions = [
            { type: 'DELETE_NEW_TAG_START' },
            { type: 'DELETE_NEW_TAG_SUCCESS' }
        ];
        const store = mockStore({
            results: initialState
        });
        await store.dispatch(actions.deleteNewTag('id'));
        expect(store.getActions()).toEqual(expectedActions)
    });

    it('fails to delete new tag', async () => {
        axios.delete.mockRejectedValueOnce('error');
        const expectedActions = [
            { type: 'DELETE_NEW_TAG_START' },
            { type: 'DELETE_NEW_TAG_FAIL', payload: { err: 'error' } }
        ];
        const store = mockStore({
            results: initialState
        });
        await store.dispatch(actions.deleteNewTag('id'));
        expect(store.getActions()).toEqual(expectedActions)
    });

    it('adds new thing successfully', async () => {
        axios.post.mockResolvedValue({ data: thingData });
        const expectedActions = [
            { type: 'ADD_THING_START' },
            { type: 'ADD_THING_SUCCESS', payload: { data: thingData } }
        ];
        const store = mockStore({
            results: initialState
        });
        await store.dispatch(actions.addThing(thingData));
        expect(store.getActions()).toEqual(expectedActions)
    });

    it('fails to add new thing', async () => {
        axios.post.mockRejectedValueOnce('error');
        const expectedActions = [
            { type: 'ADD_THING_START' },
            { type: 'ADD_THING_FAIL', payload: { err: 'error' } }
        ];
        const store = mockStore({
            results: initialState
        });
        await store.dispatch(actions.addThing(tagData));
        expect(store.getActions()).toEqual(expectedActions)
    });
});
