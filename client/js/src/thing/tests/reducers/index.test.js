import reducer, { initialState } from '../../reducers';

describe('Thing reducer', () => {
    it('should return the initial state', () => {
        expect(reducer(undefined, {})).toEqual(initialState);
    });
    it('should handle saving a new tag', () => {
        let result = reducer(undefined, { type: 'ADD_TAG_START' });
        expect(result.isSavingNewTag).toBe(true);
        result = reducer(undefined, { type: 'ADD_TAG_SUCCESS' });
        expect(result.isSavingNewTag).toBe(false);
    });
    it('should handle failing to save a new tag', () => {
        let result = reducer(undefined, { type: 'ADD_TAG_START' });
        expect(result.isSavingNewTag).toBe(true);
        result = reducer(undefined, { type: 'ADD_TAG_FAIL' });
        expect(result.isSavingNewTag).toBe(false);
    });
    it('should handle new thing being created', () => {
        const state = {
            ...initialState,
            name: 'new thing',
            tags: ['new tag'],
        };
        let result = reducer(state, { type: 'ADD_THING_START' });
        expect(result.isSavingNewThing).toBe(true);
        result = reducer(state, { type: 'ADD_THING_SUCCESS' });
        expect(result.isSavingNewTag).toBe(false);
        expect(result.name).toBe('');
        expect(result.tags).toHaveLength(0);
    });
    it('should handle failing to create a new thing', () => {
        const state = {
            ...initialState,
            name: 'new thing',
            tags: ['new tag'],
        };
        let result = reducer(state, { type: 'ADD_THING_START' });
        expect(result.isSavingNewThing).toBe(true);
        result = reducer(state, { type: 'ADD_THING_FAIL' });
        expect(result.isSavingNewTag).toBe(false);
        expect(result.name).toBe(state.name);
        expect(result.tags).toBe(state.tags);
    });
    it('should handle fetching tags', () => {
        const tags = ['tag 1', 'tag 2'];
        let result = reducer(undefined, { type: 'FETCH_TAGS_START' });
        expect(result.isFetchingTags).toBe(true);
        result = reducer(undefined, { type: 'FETCH_TAGS_SUCCESS', payload: { tags } });
        expect(result.isFetchingTags).toBe(false);
        expect(result.tags).toBe(tags);
    });
    it('should handle failing to fetch tags', () => {
        let result = reducer(undefined, { type: 'FETCH_TAGS_START' });
        expect(result.isFetchingTags).toBe(true);
        result = reducer(undefined, { type: 'FETCH_TAGS_FAIL' });
        expect(result.isFetchingTags).toBe(false);
    });
});