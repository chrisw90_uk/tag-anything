import React from 'react';
import { render, fireEvent, screen, waitFor, } from '../../../test-utils'
import '@testing-library/jest-dom/extend-expect'

import AddThing from "../../components/AddThing";

describe('AddThing', () => {
    it('allows user to specify name', () => {
        render(<AddThing />);
        const input = screen.getByLabelText('Name');
        fireEvent.change(input, { target: { value: 'test name' } });
        expect(input.value).toBe('test name');
    });
    it('clears name on successful save', async () => {
        const addThingMock = jest.fn();
        render(<AddThing addThing={addThingMock.mockReturnValue(true)}/>);
        const input = screen.getByLabelText('Name');

        fireEvent.change(input, { target: { value: 'test name' } });
        expect(input.value).toBe('test name');

        const saveBtn = screen.getByRole('button');
        fireEvent.click(saveBtn);
        await waitFor(() => expect(input.value).toBe(''));
    });
    it('does not clear name when save fails', async () => {
        const addThingMock = jest.fn();
        render(<AddThing addThing={addThingMock.mockReturnValue(false)}/>);
        const input = screen.getByLabelText('Name');

        fireEvent.change(input, { target: { value: 'test name' } });
        expect(input.value).toBe('test name');

        const saveBtn = screen.getByRole('button');
        fireEvent.click(saveBtn);
        await waitFor(() => expect(input.value).toBe('test name'));
    });
    it('disables fields when saving new thing', async () => {
        render(<AddThing isSavingNewThing />);
        const name = screen.getByLabelText('Name');
        const tags = screen.getByLabelText('Tags');
        const saveBtn = screen.getByRole('button');
        expect(name).toBeDisabled();
        expect(tags).toBeDisabled();
        expect(saveBtn).toBeDisabled();
    });
});