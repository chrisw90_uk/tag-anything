import { composeWithDevTools } from 'redux-devtools-extension';
import { combineReducers, createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import ThingReducer from "./thing/reducers";
import ResultsReducer from "./home/reducers";

export const rootReducer = combineReducers({
    'results': ResultsReducer,
    'thing': ThingReducer,
});

const composeEnhancers = composeWithDevTools({
    trace: true,
});

const store = createStore(
    rootReducer,
    composeEnhancers(applyMiddleware(thunk))
);

export default store;
