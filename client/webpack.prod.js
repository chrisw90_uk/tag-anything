const path = require('path');

module.exports = {
    entry: ['./js/src/app.js'],
    watch: false,
    mode: 'production',
    resolve: {
        extensions: ['*', '.js', '.jsx']
    },
    output: {
        filename: 'app.bundle.js',
        path: path.resolve(__dirname, 'js/dist')
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: ['babel-loader']
            },
            {
                test: /\.scss$/,
                use: [
                    "style-loader", //outputs our CSS into a <style> tag in the document.
                    "css-loader", //parses the CSS into JavaScript and resolves any dependencies.
                    "sass-loader" //transforms Sass into CSS.
                ]
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    "url-loader"
                ]
            }
        ]
    },
};
